const config = {
  version: '0.0.1',
  githubUrl: 'https://www.github.com/pep108/create-windi-mithril-app',
  gitlabUrl: 'https://www.gitlab.com/pep108/create-windi-mithril-app',
  appName: 'Create Windi Mithril App',
  API_ENDPOINT: 'https://api.example.com',
  LOGIN_LINK: 'https://login.example.com'
}

export default config
